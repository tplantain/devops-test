<?xml version="1.0" encoding="UTF-8"?>
<!-- @generated mapFile="xslt/Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1.map" md5sum="f56b62ff28e05685a69606cb42868433" version="7.0.400" -->
<!--
*****************************************************************************
*   This file has been generated by the IBM XML Mapping Editor V7.0.400
*
*   Mapping file:		Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1.map
*   Map declaration(s):	Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1
*   Input file(s):		smo://smo/name%3Dwsdl-primary/message%3D%257Bhttp%253A%252F%252Fwww.nbfg.ca%252FMF%252FPH%252FV2%257DcalculateInsurancePremiumPH_MFResponseMsg/xpath%3D%252Fbody/smo.xsd
*   Output file(s):		smo://smo/name%3Dwsdl-primary/message%3D%257Bhttp%253A%252F%252Fwww.nbfg.ca%252FAgmtProdOpers%252FCreditInsuranceAgreementMgmt%252FV2%257DcalculateMortgageCreditInsPremiumResponseMsg/xpath%3D%252Fbody/smo.xsd
*
*   Note: Do not modify the contents of this file as it is overwritten
*         each time the mapping model is updated.
*****************************************************************************
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xslt"
    xmlns:str="http://exslt.org/strings"
    xmlns:set="http://exslt.org/sets"
    xmlns:math="http://exslt.org/math"
    xmlns:exsl="http://exslt.org/common"
    xmlns:date="http://exslt.org/dates-and-times"
    xmlns:in30="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHLink_MFResponse"
    xmlns:in31="http://www.nbfg.ca/MF/PH/GetMasterAgreementPH_MFRequest"
    xmlns:in32="http://www.nbfg.ca/MF/PH/UpdateLoanAgmtPHMiscellaneous_MFRequest"
    xmlns:in="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHLatePayments_MFRequest"
    xmlns:in2="http://www.nbfg.ca/MF/PH/CreateLoanAgreementPH_MFRequest"
    xmlns:in3="http://www.nbfg.ca/MF/PH/UpdateLoanAgmtPHMiscellaneous_MFResponse"
    xmlns:in4="http://www.nbfg.ca/MF/PH/SetLoanAgreementLinkPH_MFResponse"
    xmlns:in33="http://www.nbfg.ca/MF/PH/SetRateRequest"
    xmlns:in34="http://www.nbfg.ca/MF/PH/UpdateDisbursedLoanAgreementPH_MFRequest"
    xmlns:in5="http://www.nbfg.ca/MF/PH/GetLoanAgmtPHMiscellaneous_MFResponse"
    xmlns:in6="http://www.nbfg.ca/MF/PH/RenegotiateRenewPostDatedLoanAgreementPH_MFRequest"
    xmlns:in35="http://www.nbfg.ca/MF/PH/UpdateCollateralAgreementPH_MFResponse"
    xmlns:in37="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHDetails_MFRequest"
    xmlns:in36="http://www.nbfg.ca/MF/PH/ManageLoanAgreementPHStatement_MFRequest"
    xmlns:in38="http://www.nbfg.ca/MF/PH/CancelRateResponse"
    xmlns:in39="http://www.nbfg.ca/MF/PH/CalculateInsurancePremiumPH_MFResponse"
    xmlns:in7="http://www.nbfg.ca/MF/PH/CalculateLoanAgreementPHCosts_MFResponse"
    xmlns:in40="http://www.nbfg.ca/MF/PH/RenegotiateRenewLoanAgreementPH_MFRequest"
    xmlns:in8="http://www.nbfg.ca/MF/PH/SetLoanAgreementPHDeed_MFRequest"
    xmlns:in41="http://www.nbfg.ca/MF/PH/Operations"
    xmlns:in42="http://www.nbfg.ca/MF/PH/CalculateLoanAgreementPHCosts_MFRequest"
    xmlns:in43="http://www.nbfg.ca/MF/PH/GetMasterAgreementPHStructure_MFResponse"
    xmlns:in44="http://www.nbfg.ca/MF/PH/CancelRateRequest"
    xmlns:in45="wsdl.http://www.nbfg.ca/MF/PH/V2"
    xmlns:in9="http://www.nbfg.ca/MF/PH/DeleteLoanAgreementPH_MFResponse"
    xmlns:in49="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHDeed_MFRequest"
    xmlns:in48="http://www.nbfg.ca/MF/PH/GetLoanAgreementPH_MFRequest"
    xmlns:in47="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHDetails_MFResponse"
    xmlns:in46="http://www.nbfg.ca/MF/PH/ManageLoanAgreementPHStatement_MFResponse"
    xmlns:in10="http://www.nbfg.ca/MF/PH/UpdateCollateralAgreementPH_MFRequest"
    xmlns:in50="http://www.nbfg.ca/MF/PH/RenegotiateRenewPostDatedLoanAgreementPH_MFResponse"
    xmlns:in11="http://www.nbfg.ca/MF/PH/DeleteLoanAgreementPH_MFRequest"
    xmlns:in51="http://www.nbfg.ca/MF/PH/UpdateDisbursedLoanAgreementPH_MFResponse"
    xmlns:in12="http://www.nbfg.ca/MF/PH/V2"
    xmlns:in13="http://www.nbfg.ca/MF/PH/SetRateResponse"
    xmlns:in14="http://www.nbfg.ca/MF/PH/CalculateLoanAgreementPHIndemnityBlendedRate_MFResponse"
    xmlns:in16="http://www.nbfg.ca/EnterpriseMgmt"
    xmlns:in15="http://www.nbfg.ca/MF/PH/SetLoanAgreementLinkPH_MFRequest"
    xmlns:in17="http://www.nbfg.ca/MF/PH/CreateLoanAgreementPH_MFResponse"
    xmlns:in18="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHAdditionalPayments_MFResponse"
    xmlns:in52="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHAdditionalPayments_MFRequest"
    xmlns:in53="http://www.nbfg.ca/MF/PH/GetMasterAgreementPH_MFResponse"
    xmlns:in19="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHLink_MFRequest"
    xmlns:in20="http://www.nbfg.ca/MF/PH/CalculateInsurancePremiumPH_MFRequest"
    xmlns:in21="http://www.nbfg.ca/MF/PH/SetInsuranceAgreementPH_MFResponse"
    xmlns:in54="http://www.nbfg.ca/MF/PH/UpdateLoanAgreementPH_MFResponse"
    xmlns:in22="http://www.nbfg.ca/MF/PH/UpdateLoanAgreementPHDetails_MFRequest"
    xmlns:in23="http://www.nbfg.ca/MF/PH/DeleteLoanAgreementPHLink_MFRequest"
    xmlns:in55="http://www.nbfg.ca/MF/PH/SetMasterAgreementPH_MFResponse"
    xmlns:in24="http://www.nbfg.ca/MF/PH/DeleteLoanAgreementPHLink_MFResponse"
    xmlns:in56="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHDeed_MFResponse"
    xmlns:in57="http://www.nbfg.ca/MF/PH/GetLoanAgreementPH_MFResponse"
    xmlns:in25="http://www.nbfg.ca/MF/PH/AssumeLoanAgreementPH_MFRequest"
    xmlns:in26="http://www.nbfg.ca/MF/PH/GetLoanAgmtPHMiscellaneous_MFRequest"
    xmlns:in58="http://www.nbfg.ca/MF/PH/GetInsuranceAgreementPH_MFResponse"
    xmlns:in60="http://www.nbfg.ca/MF/PH/UpdateLoanAgreementPHDetails_MFResponse"
    xmlns:in59="http://www.nbfg.ca/MF/PH/UpdateLoanAgreementPH_MFRequest"
    xmlns:in61="http://www.nbfg.ca/MF/PH/SetLoanAgreementPHDeed_MFResponse"
    xmlns:in62="http://www.nbfg.ca/MF/PH/GetLoanAgreementPHLatePayments_MFResponse"
    xmlns:in63="http://www.nbfg.ca/MF/PH/RenegotiateRenewLoanAgreementPH_MFResponse"
    xmlns:in27="http://www.nbfg.ca/MF/PH/CalculateLoanAgreementPHIndemnityBlendedRate_MFRequest"
    xmlns:in28="http://www.nbfg.ca/MF/PH/GetInsuranceAgreementPH_MFRequest"
    xmlns:in64="http://www.nbfg.ca/MF/PH/AssumeLoanAgreementPH_MFResponse"
    xmlns:in65="http://www.nbfg.ca/MF/PH/SetMasterAgreementPH_MFRequest"
    xmlns:in66="http://www.nbfg.ca/MF/PH/GetMasterAgreementPHStructure_MFRequest"
    xmlns:in29="http://www.nbfg.ca/MF/PH/SetInsuranceAgreementPH_MFRequest"
    xmlns:io4="http://www.nbfg.ca/AgmtProdOpers"
    xmlns:io5="http://www.w3.org/2003/05/soap-envelope"
    xmlns:io6="http://www.ibm.com/websphere/sibx/smo/v6.0.1"
    xmlns:out6="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/CalculateMortgageCreditInsurancePremiumRequest"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:out="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/CalculateMortgageCreditInsurancePremiumResponse"
    xmlns:io="http://www.nbfg.ca/ClientServices"
    xmlns:out2="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/GetMortgageCreditInsuranceRequest"
    xmlns:out7="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgreementMgmt/V2"
    xmlns:out8="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/SetMortgageCreditInsuranceResponse"
    xmlns:io2="http://www.ibm.com/xmlns/prod/websphere/mq/sca/6.0.0"
    xmlns:io3="http://schemas.xmlsoap.org/ws/2004/08/addressing"
    xmlns:out3="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/CreateCreditInsuranceResponse"
    xmlns:io7="http://www.ibm.com/xmlns/prod/websphere/http/sca/6.1.0"
    xmlns:out9="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/CreateCreditInsuranceRequest"
    xmlns:out4="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/Operations"
    xmlns:out10="wsdl.http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgreementMgmt/V2"
    xmlns:xsd4xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:out5="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/SetMortgageCreditInsuranceRequest"
    xmlns:io8="http://www.w3.org/2005/08/addressing"
    xmlns:out11="http://www.nbfg.ca/AgmtProdOpers/CreditInsuranceAgmtMgmt/GetMortgageCreditInsuranceResponse"
    xmlns:map="http://CreditInsuranceAgreementMgmtMed/xslt/Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1"
    xmlns:msl="http://www.ibm.com/xmlmap"
    exclude-result-prefixes="in50 in51 in52 in53 in10 in54 in11 in55 msl in56 in12 in57 in13 math in58 in14 in59 in15 in16 in17 in18 in19 xalan in60 in61 in62 in63 in64 in20 in65 in21 in66 in in22 in23 in24 in25 in26 in27 in28 in29 in2 date in3 in4 in5 str in6 in7 in8 in9 in30 in31 in32 in33 in34 in35 in36 in37 in38 in39 exsl map in40 set in41 in42 in43 in44 in45 in46 in47 in48 in49"
    version="1.0">
  <xsl:output method="xml" encoding="UTF-8" indent="no"/>

  <!-- root wrapper template  -->
  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="msl:datamap">
        <msl:datamap>
          <dataObject>
            <xsl:attribute name="xsi:type">
              <xsl:value-of select="'out10:calculateMortgageCreditInsPremiumResponseMsg'"/>
            </xsl:attribute>
            <xsl:call-template name="map:Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_12">
              <xsl:with-param name="body" select="msl:datamap/dataObject[1]"/>
            </xsl:call-template>
          </dataObject>
        </msl:datamap>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="body" mode="map:Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- This rule represents an element mapping: "body" to "body".  -->
  <xsl:template match="body"  mode="map:Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_1">
    <body>
      <xsl:attribute name="xsi:type">
        <xsl:value-of select="'out10:calculateMortgageCreditInsPremiumResponseMsg'"/>
      </xsl:attribute>
      <out7:calculateMortgageCreditInsPremiumResponse>
        <serviceResponse>
          <!-- a structural mapping: "in12:calculateInsurancePremiumPH_MFResponse/serviceResponse/in39:loanAgreement"(LoanAgreementType) to "out:loanAgreement"(LoanAgreementType) -->
          <xsl:apply-templates select="in12:calculateInsurancePremiumPH_MFResponse/serviceResponse/in39:loanAgreement" mode="localLoanAgreementToLoanAgreement_619942447"/>
        </serviceResponse>
      </out7:calculateMortgageCreditInsPremiumResponse>
    </body>
  </xsl:template>

  <!-- This rule represents a type mapping: "body" to "body".  -->
  <xsl:template name="map:Map_calculateInsurancePremiumPH_MFRes_to_calculateMortgageCreditInsPremiumRes_req_12">
    <xsl:param name="body"/>
    <out7:calculateMortgageCreditInsPremiumResponse>
      <serviceResponse>
        <!-- a structural mapping: "$body/in12:calculateInsurancePremiumPH_MFResponse/serviceResponse/in39:loanAgreement"(LoanAgreementType) to "out:loanAgreement"(LoanAgreementType) -->
        <xsl:apply-templates select="$body/in12:calculateInsurancePremiumPH_MFResponse/serviceResponse/in39:loanAgreement" mode="localLoanAgreementToLoanAgreement_619942447"/>
      </serviceResponse>
    </out7:calculateMortgageCreditInsPremiumResponse>
  </xsl:template>

  <!-- This rule represents an element mapping: "in39:loanAgreement" to "out:loanAgreement".  -->
  <xsl:template match="in39:loanAgreement"  mode="localLoanAgreementToLoanAgreement_619942447">
    <out:loanAgreement>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a simple data mapping: "io4:agmtTypeCd"(<string>) to "io4:agmtTypeCd"(<string>) -->
      <io4:agmtTypeCd>
        <xsl:value-of select="io4:agmtTypeCd"/>
      </io4:agmtTypeCd>
      <!-- a simple data mapping: "io4:acctNo"(<string>) to "io4:acctNo"(<string>) -->
      <xsl:if test="io4:acctNo">
        <io4:acctNo>
          <xsl:value-of select="io4:acctNo"/>
        </io4:acctNo>
      </xsl:if>
      <!-- a structural mapping: "in39:agreementIndividualList"(AgreementIndividualListType) to "out:agreementIndividualList"(AgreementIndividualListType) -->
      <xsl:apply-templates select="in39:agreementIndividualList" mode="localAgreementIndividualListToAgreementIndividualList_1523917001"/>
    </out:loanAgreement>
  </xsl:template>

  <!-- This rule represents an element mapping: "in39:agreementIndividualList" to "out:agreementIndividualList".  -->
  <xsl:template match="in39:agreementIndividualList"  mode="localAgreementIndividualListToAgreementIndividualList_1523917001">
    <out:agreementIndividualList>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a for-each transform: "in39:agreementIndividual"(AgreementIndividualType) to "out:agreementIndividual"(AgreementIndividualType) -->
      <xsl:apply-templates select="in39:agreementIndividual" mode="localAgreementIndividualToAgreementIndividual_1150322759"/>
    </out:agreementIndividualList>
  </xsl:template>

  <!-- This rule represents a for-each transform: "in39:agreementIndividual" to "out:agreementIndividual".  -->
  <xsl:template match="in39:agreementIndividual"  mode="localAgreementIndividualToAgreementIndividual_1150322759">
    <out:agreementIndividual>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a simple data mapping: "io:memberIdNo"(<string>) to "io:memberIdNo"(<string>) -->
      <xsl:if test="io:memberIdNo">
        <io:memberIdNo>
          <xsl:value-of select="io:memberIdNo"/>
        </io:memberIdNo>
      </xsl:if>
      <!-- a simple data mapping: "io:ptyAgmtRltnpTypeCd"(<string>) to "io:ptyAgmtRltnpTypeCd"(<string>) -->
      <io:ptyAgmtRltnpTypeCd>
        <xsl:value-of select="io:ptyAgmtRltnpTypeCd"/>
      </io:ptyAgmtRltnpTypeCd>
      <!-- a simple data mapping: "io:srcCd"(<string>) to "io:srcCd"(<string>) -->
      <xsl:if test="io:srcCd">
        <io:srcCd>
          <xsl:value-of select="io:srcCd"/>
        </io:srcCd>
      </xsl:if>
      <!-- a simple data mapping: "io:indvBirthDt"(<date>) to "io:indvBirthDt"(<date>) -->
      <io:indvBirthDt>
        <xsl:value-of select="io:indvBirthDt"/>
      </io:indvBirthDt>
      <!-- a structural mapping: "in39:insuranceAgreementList"(InsuranceAgreementListType) to "out:insuranceAgreementList"(InsuranceAgreementListType) -->
      <xsl:apply-templates select="in39:insuranceAgreementList" mode="localInsuranceAgreementListToInsuranceAgreementList_1883260625"/>
    </out:agreementIndividual>
  </xsl:template>

  <!-- This rule represents an element mapping: "in39:insuranceAgreementList" to "out:insuranceAgreementList".  -->
  <xsl:template match="in39:insuranceAgreementList"  mode="localInsuranceAgreementListToInsuranceAgreementList_1883260625">
    <out:insuranceAgreementList>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a for-each transform: "in39:insuranceAgreement"(InsuranceAgreementType) to "out:insuranceAgreement"(InsuranceAgreementType) -->
      <xsl:apply-templates select="in39:insuranceAgreement" mode="localInsuranceAgreementToInsuranceAgreement_1596604495"/>
    </out:insuranceAgreementList>
  </xsl:template>

  <!-- This rule represents a for-each transform: "in39:insuranceAgreement" to "out:insuranceAgreement".  -->
  <xsl:template match="in39:insuranceAgreement"  mode="localInsuranceAgreementToInsuranceAgreement_1596604495">
    <out:insuranceAgreement>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a simple data mapping: "io4:agmtTypeCd"(<string>) to "io4:agmtTypeCd"(<string>) -->
      <io4:agmtTypeCd>
        <xsl:value-of select="io4:agmtTypeCd"/>
      </io4:agmtTypeCd>
      <!-- a structural mapping: "in39:agreementDetailInformation"(AgreementDetailInformationType) to "out:agreementDetailInformation"(AgreementDetailInformationType) -->
      <xsl:apply-templates select="in39:agreementDetailInformation" mode="localAgreementDetailInformationToAgreementDetailInformation_1107348619"/>
    </out:insuranceAgreement>
  </xsl:template>

  <!-- This rule represents an element mapping: "in39:agreementDetailInformation" to "out:agreementDetailInformation".  -->
  <xsl:template match="in39:agreementDetailInformation"  mode="localAgreementDetailInformationToAgreementDetailInformation_1107348619">
    <out:agreementDetailInformation>
      <!-- a simple data mapping: "@schemaVersion"(decimal) to "schemaVersion"(decimal) -->
      <xsl:if test="@schemaVersion">
        <xsl:attribute name="schemaVersion">
          <xsl:value-of select="@schemaVersion"/>
        </xsl:attribute>
      </xsl:if>
      <!-- a simple data mapping: "io4:agmtDetailInfoCurrencyCd"(<string>) to "io4:agmtDetailInfoCurrencyCd"(<string>) -->
      <io4:agmtDetailInfoCurrencyCd>
        <xsl:value-of select="io4:agmtDetailInfoCurrencyCd"/>
      </io4:agmtDetailInfoCurrencyCd>
      <!-- a simple data mapping: "io4:agmtDetailInfoUOMCd"(<string>) to "io4:agmtDetailInfoUOMCd"(<string>) -->
      <io4:agmtDetailInfoUOMCd>
        <xsl:value-of select="io4:agmtDetailInfoUOMCd"/>
      </io4:agmtDetailInfoUOMCd>
      <!-- a simple data mapping: "io4:agmtDetailInfoTypeCd"(<string>) to "io4:agmtDetailInfoTypeCd"(<string>) -->
      <io4:agmtDetailInfoTypeCd>
        <xsl:value-of select="io4:agmtDetailInfoTypeCd"/>
      </io4:agmtDetailInfoTypeCd>
      <!-- a simple data mapping: "io4:agmtDetailInfoValue"(<decimal>) to "io4:agmtDetailInfoValue"(<decimal>) -->
      <io4:agmtDetailInfoValue>
        <xsl:value-of select="io4:agmtDetailInfoValue"/>
      </io4:agmtDetailInfoValue>
    </out:agreementDetailInformation>
  </xsl:template>

  <!-- *****************    Utility Templates    ******************  -->
  <!-- copy the namespace declarations from the source to the target -->
  <xsl:template name="copyNamespaceDeclarations">
    <xsl:param name="root"/>
    <xsl:for-each select="$root/namespace::*">
      <xsl:copy/>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
